title: IntelliJ IDEA
---

# IntelliJ IDEA

## Download

Download IntelliJ __Community Edition__ [di sini](https://www.jetbrains.com/idea/download/#section=windows).

## Preferensi 

### Masuk ke Project Defaults

![alt text](images/landpage_menu_project_defaults.png "project defaults")

### Setting JDK

Pilih __New__, lalu arahkan ke folder JDK, contoh: `C:\Program Files\Java\jdk1.8.0_171`.

![alt text](images/select_sdk.png "select sdk")

