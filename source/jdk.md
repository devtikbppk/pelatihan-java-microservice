title: JDK
---

# Java Development Kit

## Cek Instalasi

Cek instalasi dengan `java -version`. 

```powershell
PS C:\Users\User> java -version
java version "1.8.0_171"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
```

Versi yang diperlukan adalah `1.8.xxxx`. 

## Download

Apabila versi yang ada kurang dari `1.8.xxxx`, download [disini](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

