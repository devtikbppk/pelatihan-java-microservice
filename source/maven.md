title: Maven
---

# Maven

## Instalasi via Chocolatey (Recommended)

Install `chocolatey` dengan mengikuti petunjuk [ini](https://chocolatey.org/install).

Setelah `chocolatey` terinstall, jalankan perintah `choco install maven`.

### Cek instalasi maven

```powershell
PS C:\Users\User> mvn -version
Apache Maven 3.5.2 (138edd61fd100ec658bfa2d307c43b76940a5d7d; 2017-10-18T14:58:13+07:00)
Maven home: C:\Program Files\apache\maven\bin\..
Java version: 1.8.0_171, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_171\jre
Default locale: en_US, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```


## Instalasi Manual

Apabila sudah melakukan instalasi dengan `chocolatey`, instalasi manual ini tidak perlu dilakukan.

1. Download `maven` [disini](https://drive.google.com/file/d/1wzoiGH19GZQcARkYZd0x0IcbpYl98kg2/view?usp=sharing)
2. Lakukan instalasi sesuai petunjuk link [ini](https://www.baeldung.com/install-maven-on-windows-linux-mac).

> Skenario apabila koneksi bermasalah

1. Download `settings.xml` [disini](https://drive.google.com/file/d/1T6kygf-cxPJxlFpJcySFM12DPYjUUbWY/view?usp=sharing)
2. __Windows__, copy file ke `C:\Users\{username-anda}\.m2`
3. __Mac__, copy file ke `~/.m2`